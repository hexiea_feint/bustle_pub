# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Use Slime for templating instead of eex
config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

# Configures the endpoint
config :bustle_pub, BustlePubWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ks+wbpdUsVReqHoOK7cTEj+Yxuf8ZVRElPb/eTrMhMqW06LHfiIt0QwqvTvNP6tu",
  render_errors: [view: BustlePubWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BustlePub.PubSub, adapter: Phoenix.PubSub.PG2]

config :bustle_pub, activity_context: "https://www.w3.org/ns/activitystreams"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
