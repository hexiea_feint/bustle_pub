defmodule BustlePub.ActivityStreams.JasonImpls do
  require Logger

  defmacro __using__(_) do
    quote do
      defimpl Jason.Encoder, for: __MODULE__ do
        def encode(strct, opts \\ %{}) do
          strct
          |> BustlePub.ActivityStreams.purge_nil()
          |> Jason.Encoder.encode(opts)
        end
      end
    end
  end
end
