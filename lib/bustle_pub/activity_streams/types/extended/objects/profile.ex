defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Profile do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:describes])
end
