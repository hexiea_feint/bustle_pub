defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Place do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:accuracy, :altitude, :latitude, :longitude, :radius, :units])
end
