defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Mention do
  alias BustlePub.ActivityStreams.Types.Core.Link
  import BustlePub.Extendable
  use BustlePub.ActivityStreams.JasonImpls

  extend(Link)
end
