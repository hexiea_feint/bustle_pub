defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Page do
  alias BustlePub.ActivityStreams.Types.Extended.Objects.Document
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Document)
end
