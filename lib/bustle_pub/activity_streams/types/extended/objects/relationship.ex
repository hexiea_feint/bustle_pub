defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Relationship do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:subject, :object, :relationship])
end
