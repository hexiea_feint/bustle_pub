defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Image do
  alias BustlePub.ActivityStreams.Types.Extended.Objects.Document
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Document)
end
