defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Event do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object)
end
