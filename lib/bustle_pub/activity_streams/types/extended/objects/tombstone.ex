defmodule BustlePub.ActivityStreams.Types.Extended.Objects.Tombstone do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:former_type, :deleted])
end
