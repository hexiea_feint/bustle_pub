defmodule BustlePub.ActivityStreams.Types.Extended.Actors.Service do
  alias BustlePub.ActivityStreams.Types.Actor
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Actor)
end
