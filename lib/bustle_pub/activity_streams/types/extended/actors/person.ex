defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Person do
  alias BustlePub.ActivityStreams.Types.Actor
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Actor)
end
