defmodule BustlePub.ActivityStreams.Types.Extended.Activities.TentativeReject do
  alias BustlePub.ActivityStreams.Types.Extended.Activities.Reject
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Reject)
end
