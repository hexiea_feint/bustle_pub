defmodule BustlePub.ActivityStreams.Types.Extended.Activities.TentativeAccept do
  alias BustlePub.ActivityStreams.Types.Extended.Activities.Accept
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Accept)
end
