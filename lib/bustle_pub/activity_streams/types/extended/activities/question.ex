defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Question do
  alias BustlePub.ActivityStreams.Types.Core.IntransitiveActivity
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(IntransitiveActivity, [:one_of, :any_of, :closed])
end
