defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Update do
  alias BustlePub.ActivityStreams.Types.Activity
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Activity)
end
