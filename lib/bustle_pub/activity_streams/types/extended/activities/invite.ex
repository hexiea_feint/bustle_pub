defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Invite do
  alias BustlePub.ActivityStreams.Types.Extended.Activities.Offer
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Offer)
end
