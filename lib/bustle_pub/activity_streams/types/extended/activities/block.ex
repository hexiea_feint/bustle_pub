defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Block do
  alias BustlePub.ActivityStreams.Types.Extended.Activities.Ignore
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Ignore)
end
