defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Dislike do
  alias BustlePub.ActivityStreams.Types.Activity
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Activity)
end
