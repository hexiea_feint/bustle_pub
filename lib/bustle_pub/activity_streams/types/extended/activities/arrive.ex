defmodule BustlePub.ActivityStreams.Types.Extended.Activities.Arrive do
  alias BustlePub.ActivityStreams.Types.Core.IntransitiveActivity
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(IntransitiveActivity)
end
