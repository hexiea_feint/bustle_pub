defmodule BustlePub.ActivityStreams.Types.Actor do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [
    :inbox,
    :outbox,
    :following,
    :followers,
    :liked,
    :streams,
    :preferred_username,
    :endpoints
  ])
end
