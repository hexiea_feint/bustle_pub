defmodule BustlePub.ActivityStreams.Types.Activity do
  alias BustlePub.ActivityStreams.Types.Object
  import BustlePub.Extendable

  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:actor, :object, :target, :result, :origin, :instrument])
end
