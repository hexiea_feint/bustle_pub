defmodule BustlePub.ActivityStreams.Types.Object do
  # Base ActivityStreams context
  use BustlePub.ActivityStreams.JasonImpls

  @enforce_keys [:id, :type]
  @derive BustlePub.ActivityStreams.ActivityRecord
  defstruct [
    :context,
    :id,
    :type,
    :source,
    :attachment,
    :attributed_to,
    :audience,
    :content,
    :name,
    :end_time,
    :generator,
    :icon,
    :image,
    :in_reply_to,
    :location,
    :preview,
    :published,
    :replies,
    :start_time,
    :summary,
    :tag,
    :updated,
    :url,
    :to,
    :bto,
    :cc,
    :bcc,
    :media_type,
    :duration,
    "@context": Application.fetch_env!(:bustle_pub, :activity_context)
  ]
end
