defmodule BustlePub.ActivityStreams.Types.Core.CollectionPage do
  alias BustlePub.ActivityStreams.Types.Core.Collection
  import BustlePub.Extendable

  
  use BustlePub.ActivityStreams.JasonImpls

  extend(Collection, [:part_of, :next, :prev])
end
