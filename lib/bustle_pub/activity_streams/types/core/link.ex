defmodule BustlePub.ActivityStreams.Types.Core.Link do
  use BustlePub.ActivityStreams.JasonImpls

  @enforce_keys [:id, :media_type, :"@context"]
  defstruct [
    :id,
    :name,
    # Not explicitly mentioned, but is here for completeness
    :type,
    :href,
    :rel,
    :media_type,
    :name,
    :href_lang,
    :height,
    :width,
    :preview,
    "@context": Application.fetch_env!(:bustle_pub, :activity_context)
  ]

  @type t :: %__MODULE__{id: String.t(), media_type: String.t()}
end
