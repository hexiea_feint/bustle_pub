defmodule BustlePub.ActivityStreams.Types.Core.OrderedCollection do
  alias BustlePub.ActivityStreams.Types.Core.Collection
  import BustlePub.Extendable

  use BustlePub.ActivityStreams.JasonImpls

  extend(Collection)

  defdelegate new(), to: Collection, as: :new
  defdelegate new(map), to: Collection, as: :new
  defdelegate empty?(collection), to: Collection, as: :empty?
  defdelegate push(collection, object), to: Collection, as: :prepend
  defdelegate pop(collection), to: Collection, as: :pop_back
end
