defmodule BustlePub.ActivityStreams.Types.Core.Collection do
  alias BustlePub.ActivityStreams.Types.Object
  alias BustlePub.ActivityStreams.Types.Core.Collection

  import BustlePub.Extendable
  use BustlePub.ActivityStreams.JasonImpls

  extend(Object, [:total_items, :current, :first, :last, :items])

  def new() do
    %Collection{
      id: -1,
      type: "Collection",
      total_items: 0,
      items: [],
      "@context": Application.fetch_env!(:bustle_pub, :activity_context)
    }
  end

  def new(list) when is_list(list) do
    %{new() | items: list}
  end

  def new(map) do
    new()
    |> Map.merge(map)
  end

  def empty?(%Collection{total_items: total, items: items}) do
    total == 0 && items != []
  end

  def prepend(collection, object) do
    %Collection{total_items: total, last: last, items: items} = collection

    case total do
      0 ->
        Collection.new(%{
          total_items: 1,
          current: object,
          first: object,
          last: object,
          items: [object]
        })

      _ ->
        Collection.new(%{
          total_items: total + 1,
          current: object,
          first: object,
          last: last,
          items: [object | items]
        })
    end
  end

  def append(collection, object) do
    %Collection{total_items: total, first: first, items: items} = collection

    case total do
      0 ->
        Collection.new(%{
          total_items: 1,
          current: object,
          first: object,
          last: object,
          items: [object]
        })

      _ ->
        Collection.new(%{
          total_items: total + 1,
          current: first,
          first: first,
          last: object,
          items: [items | object]
        })
    end
  end

  def pop_front(collection) do
    %Collection{total_items: total, items: items} = collection

    case total do
      0 ->
        raise ArgumentError, message: "Invalid collection to pop front"

      _ ->
        [popped | rest] = items

        case rest do
          [] ->
            {popped, Collection.new()}

          _ ->
            [new_first | new_rest] = rest

            case new_rest do
              [] ->
                {popped,
                 Collection.new(%{
                   total_items: 1,
                   current: new_first,
                   first: new_first,
                   last: new_first,
                   items: [new_first]
                 })}

              _ ->
                {popped,
                 Collection.new(%{
                   total_items: total - 1,
                   current: new_first,
                   first: new_first,
                   last: List.last(new_rest),
                   items: rest
                 })}
            end
        end
    end
  end

  def pop_back(collection) do
    %Collection{total_items: total, current: current, first: first, items: items} = collection

    case total do
      0 ->
        raise ArgumentError, message: "Invalid collection to pop back"

      _ ->
        [popped | rest] = items

        case rest do
          [] ->
            {popped, Collection.new()}

          _ ->
            new_last = List.last(rest)

            {popped,
             Collection.new(%{
               total_items: total - 1,
               current: current,
               first: first,
               last: new_last,
               items: rest
             })}
        end
    end
  end
end
