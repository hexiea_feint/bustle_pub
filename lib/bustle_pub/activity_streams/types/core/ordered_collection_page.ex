defmodule BustlePub.ActivityStreams.Types.Core.OrderedCollectionPage do
  alias BustlePub.ActivityStreams.Types.Core.{CollectionPage, OrderedCollection}
  import BustlePub.Extendable


  use BustlePub.ActivityStreams.JasonImpls

  extend([OrderedCollection, CollectionPage], [:start_index])
end
