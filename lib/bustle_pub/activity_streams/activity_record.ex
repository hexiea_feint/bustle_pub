defprotocol BustlePub.ActivityStreams.ActivityRecord do
  @fallback_to_any true
  @doc "Creates a new ActivityRecord with optional data from a map"
  @spec new(map) :: term
  def new(map)
  @doc "Updates an ActivityRecord with data from a map"
  @spec update(term, map) :: term
  def update(record, map)
end

defimpl BustlePub.ActivityStreams.ActivityRecord, for: Any do
  defmacro __deriving__(module, strct, _options) do
    quote do
      defimpl BustlePub.ActivityStreams.ActivityRecord, for: unquote(module) do
        def new(%{__struct__: unquote(module)} = record) do
          object_type = List.last(String.split("#{unquote(module)}", "."))

          %{
            unquote(Macro.escape(strct))
            | id: -1,
              type: object_type
          }
        end

        def update(%{__struct__: unquote(module)} = record, map) do
          record
          |> Map.merge(map)
        end
      end
    end
  end

  def new(a), do: {:error, "NOT IMPLEMENTED FOR: #{IO.inspect(a)}"}
  def update(a, b), do: {:error, "Not implemented!"}
end
