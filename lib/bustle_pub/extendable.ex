defmodule BustlePub.Extendable do
  @moduledoc """
  Module to allow ad-hoc object extension by combining all structs
  """
  defmacrop defaults() do
    ["@context": Application.fetch_env!(:bustle_pub, :activity_context), type: "#{__MODULE__}"]
  end

  defmacro extend(structs_to_extend) when is_list(structs_to_extend) do
    quote do
      defstruct Enum.concat(
                  Enum.map(
                    unquote(structs_to_extend),
                    fn s -> Map.to_list(struct(s)) end
                  )
                ) ++ unquote(defaults())

      @type t :: %__MODULE__{id: String.t(), type: String.t()}
    end
  end

  defmacro extend(structs_to_extend, extended_members) when is_list(structs_to_extend) do
    quote do
      defstruct Enum.concat(
                  Enum.map(
                    unquote(structs_to_extend),
                    fn s -> Map.to_list(struct(s)) end
                  )
                ) ++
                  unquote(extended_members) ++
                  unquote(defaults())

      @type t :: %__MODULE__{id: String.t(), type: String.t()}
    end
  end

  defmacro extend(struct_to_extend) do
    quote do
      defstruct Map.to_list(struct(unquote(struct_to_extend))) ++ unquote(defaults())

      @type t :: %__MODULE__{id: String.t(), type: String.t()}
    end
  end

  defmacro extend(struct_to_extend, extended_members) do
    quote do
      defstruct Map.to_list(struct(unquote(struct_to_extend))) ++
                  unquote(extended_members) ++
                  unquote(defaults())

      @type t :: %__MODULE__{id: String.t(), type: String.t()}
    end
  end
end
