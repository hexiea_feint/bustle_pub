defmodule BustlePub.RecordBox do
  use GenServer
  alias BustlePub.ActivityStreams.Types.Core.OrderedCollection

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default)
  end

  def deliver(pid, record) do
    GenServer.call(pid, {:deliver, record})
  end

  def poll(pid) do
    GenServer.call(pid, {:poll})
  end

  def pull(pid) do
    GenServer.call(pid, {:pull})
  end

  def delete(pid) do
    GenServer.call(pid, {:delete})
  end

  def pull_and_delete(pid) do
    GenServer.call(pid, {:pull_and_delete})
  end

  @impl true
  def init(list) when is_list(list) do
    {:ok, OrderedCollection.new(list)}
  end

  @impl true
  def init(map) when map == %{} do
    {:ok, OrderedCollection.new()}
  end

  @impl true
  def init(map) when map != %{} and map != [] do
    {:ok, OrderedCollection.new(map)}
  end

  @impl true
  def handle_call({:deliver, record}, _sender, state) do
    {:reply, :ok, OrderedCollection.push(state, record)}
  end

  @impl true
  def handle_call(:poll, _sender, state) do
    {:reply, OrderedCollection.empty?(state), state}
  end

  @impl true
  def handle_call(:pull, _sender, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call(:delete, _sender, _state) do
    {:reply, :ok, OrderedCollection.new()}
  end

  @impl true
  def handle_call(:pull_and_delete, _sender, state) do
    {:reply, state, OrderedCollection.new()}
  end
end
