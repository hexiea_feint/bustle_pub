defmodule BustlePub.ActivityStreams do
  def purge_nil(strct) do
    name = strct.__struct__

    strct
    |> Map.from_struct()
    |> Enum.reject(fn kv ->
      case kv do
        {_, ^name} ->
          true

        {_, nil} ->
          true

        _ ->
          false
      end
    end)
    |> Map.new()
  end
end
