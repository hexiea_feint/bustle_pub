defmodule BustlePub.UserActor do
  use GenServer
  alias BustlePub.ActivityStreams.Types.Actor

  def start_link(actor) do
    GenServer.start_link(__MODULE__, actor)
  end

  def add_to_outbox(record) do
    GenServer.call(:outbox, {:deliver, record})
  end

  def receive(record) do
    GenServer.call(:inbox, {:deliver, record})
  end

  def like(record) do
    GenServer.call(:likes, {:deliver, record})
  end

  def follow(record) do
    GenServer.call(:following, {:deliver, record})
  end

  def followed_by(record) do
    GenServer.call(:followers, {:deliver, record})
  end

  @impl true
  def init(_params) do
    {:ok, BustlePub.ActivityStreams.ActivityRecord.new(Actor.t())}
  end
end
