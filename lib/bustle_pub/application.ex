defmodule BustlePub.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      BustlePubWeb.Endpoint,
      BustlePub.UserActor,
      Supervisor.child_spec({BustlePub.RecordBox, []}, id: :inbox),
      Supervisor.child_spec({BustlePub.RecordBox, []}, id: :outbox),
      Supervisor.child_spec({BustlePub.RecordBox, []}, id: :following),
      Supervisor.child_spec({BustlePub.RecordBox, []}, id: :followers),
      Supervisor.child_spec({BustlePub.RecordBox, []}, id: :liked)
      # Starts a worker by calling: BustlePub.Worker.start_link(arg)
      # {BustlePub.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BustlePub.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BustlePubWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
