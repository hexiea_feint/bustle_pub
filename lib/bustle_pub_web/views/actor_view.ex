defmodule BustlePubWeb.ActorView do
  use BustlePubWeb, :view

  def render("index.json", _params) do
    %BustlePub.ActivityStreams.Types.Object{id: "a", type: "b"}
  end
end
