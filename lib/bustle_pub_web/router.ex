defmodule BustlePubWeb.Router do
  use BustlePubWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
  end

  scope "/", BustlePubWeb do
    pipe_through :api

    get "/", ActorController, :index
    get "/inbox", ActorController, :inbox
    get "/outbox", ActorController, :outbox
    get "/following", ActorController, :following
    get "/followers", ActorController, :followers
    get "/liked", ActorController, :liked
  end

  # Other scopes may use custom stacks.
  # scope "/api", BustlePubWeb do
  #   pipe_through :api
  # end
end
