defmodule BustlePubWeb.ActorController do
  use BustlePubWeb, :controller

  def index(conn, _params) do
    render(conn, "index.json")
  end

  def inbox(conn, _params) do
    render(conn, "inbox.json")
  end

  def outbox(conn, _params) do
    render(conn, "outbox.json")
  end

  def following(conn, _params) do
    render(conn, "following.json")
  end

  def followers(conn, _params) do
    render(conn, "followers.json")
  end

  def liked(conn, _params) do
    render(conn, "liked.json")
  end
end
